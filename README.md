# OpenML dataset: kickstarter_projects

https://www.openml.org/d/42076

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data are collected from Kickstarter Platform

You'll find most useful data for project analysis. Columns are self explanatory except:

usd_pledged: conversion in US dollars of the pledged column (conversion done by kickstarter).

usd pledge real: conversion in US dollars of the pledged column (conversion from Fixer.io API).

usd goal real: conversion in US dollars of the goal column (conversion from Fixer.io API).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42076) of an [OpenML dataset](https://www.openml.org/d/42076). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42076/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42076/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42076/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

